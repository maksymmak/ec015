pageextension 50111 PostedSalesInvoiceSubform extends 133
{
    layout
    {
        addlast(Control1)
        {
            field("Next recurring date"; "Next recurring date")
            {
                Caption = 'Next Recurring Date';
            }
            field("Standard Sales Code"; "Standard Sales Code")
            {
                Caption = 'Standard Sales Code';
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }

}