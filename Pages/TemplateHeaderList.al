page 50103 "Template Subscriptions List"
{
    PageType = List;
    // PageType = Worksheet;
    Caption = 'Templates';
    SourceTable = 170;
    ApplicationArea = Suite;
    // InsertAllowed = false;
    // ModifyAllowed = false;
    Editable = false;
    UsageCategory = Administration;

    AdditionalSearchTermsML = ENU = 'recurring sales,reorder,repeat sales',
                              ESM = 'ventas peri¢dicas,reordenar,repetir ventas',
                              FRC = 'ventes r‚currentes,r‚appro.,ventes r‚p‚t‚es',
                              ENC = 'recurring sales,reorder,repeat sales';
    CardPageID = "Standard Sales Code Card";
    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field(Code; Code)
                {
                    ApplicationArea = Suite;
                    ToolTip = 'Specifies a code which identifies this standard sales code.';

                }
                field(Description; Description)
                {
                    ApplicationArea = Suite;
                    ToolTip = 'Specifies a description of the standard sales code.';
                }
                field("Currency Code"; "Currency Code")
                {
                    ApplicationArea = Suite;
                    ToolTip = 'Specifies the currency code of the amounts on the standard sales lines.';
                }

                field("Maintenance Percentage"; "Maintenance Percentage")
                {
                    Caption = 'Maintenance Percentage';
                }
                field(Template; Template)
                {
                    Caption = 'Template';

                }
            }
        }
        area(FactBoxes)
        {
            systempart(Control1900383207; Links)
            {
                Visible = false;
                ApplicationArea = RecordLinks;
            }
            systempart(Control1905767507; Notes)
            {
                Visible = false;
                ApplicationArea = Notes;
            }
        }

    }

    trigger OnOpenPage()
    var
        StdSalesCode: Record 170;
    begin
        FilterGroup(2);
        Rec.SetRange(Template, True);
        FilterGroup(0);
        StdSalesCode.SetInsertAllowed(true);
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        myInt: Integer;
    begin
        SetInsertAllowed(true);
        Template := true;
        Modify(true);
    end;

}