pageextension 50101 StandardSalesCodeCardExtension extends 170
{

    layout
    {
        addlast(General)
        {
            field("Maintenance Percentage"; "Maintenance Percentage")
            {
                Caption = 'Maintenance Percentage';
                trigger OnValidate()
                var
                    TemplateLines: Record 171;
                begin

                    TemplateLines.SetRange("Standard Sales Code", Code);
                    if TemplateLines.FindSet then begin
                        //     TemplateLines.ModifyAll("Maintenance Percentage", "Maintenance Percentage");
                        repeat
                            TemplateLines."Maintenance Percentage" := rec."Maintenance Percentage";
                            TemplateLines.Modify(true);
                            if "Maintenance Percentage" <> 0 then
                                TemplateLines."Amount Excl. VAT" := (TemplateLines."Unit Price" * ("Maintenance Percentage" / 100)) * TemplateLines.Quantity
                            else
                                TemplateLines."Amount Excl. VAT" := TemplateLines."Unit Price" * TemplateLines.Quantity;
                            TemplateLines.Modify(true);

                        until TemplateLines.Next() = 0
                    end;
                end;
            }
            field(Template; Template)
            {
                Caption = 'Template';
                Editable = false;

            }

        }
        modify(Code)
        {
            trigger OnBeforeValidate()
            var
                myInt: Integer;
            begin
                if StrLen(Rec.Code) > 2 then
                    Error('Template code should not exceed 2 digits');
            end;
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    var
    begin
        SetInsertAllowed(true);
        FilterGroup(2);
        Evaluate(template, GetFilter(Template));
        Modify(true);
    end;
}