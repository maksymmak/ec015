pageextension 50104 StandardSalesCodeSubformExt extends 171
{
    layout
    {
        addlast(Control1)
        {
            field("Unit Price"; "Unit Price")
            {
                Caption = 'Unit Price';
                Editable = false;
            }
            field("Maintenance Percentage";
            "Maintenance Percentage")
            {
                Caption = 'Maintenance Percentage';
                Editable = false;
                ShowMandatory = true;
                NotBlank = true;

            }
            field("Customer Discount Group"; "Customer Discount Group")
            {
                Caption = 'Customer Discount Group';
                Editable = false;
            }
            field("Valid From Date"; "Valid From Date")
            {
                Caption = 'Valid From Date';
                Visible = isVisible;
                Editable = isEditable;
            }
            field("Valid to Date"; "Valid to Date")
            {
                Caption = 'Valid to Date';
                Visible = isVisible;
                Editable = isEditable;
            }
            field("Recurring Frequency"; "Recurring Frequency")
            {
                Caption = 'Recurring Frequency';
                Visible = isVisible;
                //Editable = isEditable;
                Editable = isBlocked;
            }
            field("Last Invoice Date"; "Last Invoice Date")
            {
                Caption = 'Last Invoice Date';
                Visible = isVisible;
                Editable = false;
            }
            field("Next Invoice Date"; "Next Invoice Date")
            {
                Caption = 'Next Invoice Date';
                Visible = isVisible;
                Editable = false;
            }
            field("Header Frequency"; "Header Frequency")
            {
                Caption = 'Header Frequency';
                Visible = isVisible;
                Editable = isEditable;
                trigger OnValidate()
                var
                    ContractHeader: Record 172;
                begin
                    isBlocked := not "Header Frequency";
                    if "Header Frequency" then begin
                        ContractHeader.SetRange(Code, "Standard Sales Code");
                        if ContractHeader.FindFirst() then
                            Rec."Recurring Frequency" := ContractHeader."Recurring Frequency"
                    end;
                end;
            }
            field(Blocked; Blocked)
            {
                Caption = 'Blocked';
                Editable = isEditable;

                trigger OnValidate()
                var
                    ContractHeader: Record 172;
                begin
                    //get contract header
                    ContractHeader.SetRange(Code, rec."Standard Sales Code");
                    if ContractHeader.FindFirst and ContractHeader.Blocked then
                        rec.Blocked := true;

                end;
            }

        }
        modify("No.")
        {
            trigger OnAfterValidate()
            var
                Header: Record 172;
                Item: Record 27;
                StandardSalesCode: Record 170;
            begin
                if Item.Get("No.") then
                    "Unit Price" := Item."Unit Price";
                if StandardSalesCode.Get("Standard Sales Code") then
                    "Maintenance Percentage" := StandardSalesCode."Maintenance Percentage";
                Header.SetRange(Code, "Standard Sales Code");
                if Header.FindFirst() then begin
                    "Recurring Frequency" := Header."Recurring Frequency";
                    "Customer Discount Group" := Header."Customer Discount Group";
                    "Valid From Date" := Header."Valid From date";
                    "Valid to Date" := Header."Valid To date";
                    "Maintenance Percentage" := Header."Maintenance Percentage";
                    Blocked := Header.Blocked;
                    "Header Frequency" := true;
                    if Item.Get("No.") then
                        "Unit Price" := Item."Unit Price";
                end;
            end;
        }


    }

    actions
    {
        addfirst(Processing)
        {
            action("Adjust Next Recurring Date")
            {
                //Caption = 'Adjust Next Recurring Date';
                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                ToolTip = 'Adjust next recurring date on posted document, this will change next invoice date for selected line';
                Image = ChangeDates;
                Visible = isEditable;
                trigger OnAction()
                var
                    SalesInvoiceLines: Record 113;
                    AskWindow: Dialog;
                    NewDate: Date;
                    DialogPage: page 50101;
                begin

                    if rec."Header Frequency" then
                        Error('Date can`t be adjusted for the line with Header Frequency');
                    if DialogPage.RunModal = Action::OK then begin
                        NewDate := DialogPage.GetDate();
                        if not (NewDate <> 0D) then
                            Error('New date should not be empty');
                        SalesInvoiceLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
                        SalesInvoiceLines.SetRange("Standard Sales Line No.", rec."Line No.");
                        if SalesInvoiceLines.FindFirst then begin
                            SalesInvoiceLines.Validate("Next recurring date", NewDate);
                            SalesInvoiceLines.Modify(true);
                            Message('Date has been adjusted for Posted Document No: %1, Line No: %2. New recurring date is %3', SalesInvoiceLines."Document No.", SalesInvoiceLines."Line No.", NewDate);
                        end
                        else
                            Error('Posted invoice was not found');
                    end

                end;

            }
        }

    }
    trigger OnOpenPage()
    var
        TemplateHeader: Record 170;
    begin
        isVisible := true;
        isEditable := true;
        isBlocked := true;
    end;

    trigger OnAfterGetCurrRecord()
    var
        TemplateHeader: Record 170;

    begin
        if TemplateHeader.Get(Rec."Standard Sales Code") and (TemplateHeader.Template = true) then begin
            isEditable := false;
            isBlocked := false;
        end;

    end;

    trigger OnAfterGetRecord()
    var
        ContractHeader: Record 172;
    begin

        isBlocked := not "Header Frequency";
    end;

    var
        isVisible: Boolean;
        isEditable: Boolean;
        isBlocked: Boolean;

}