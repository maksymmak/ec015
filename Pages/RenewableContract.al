page 50102 "Renewable Contracts"
{
    PageType = Worksheet;
    UsageCategory = Tasks;
    SourceTable = 171;
    SourceTableView = where(Blocked = filter(false));
    ShowFilter = false;
    Editable = true;
    InsertAllowed = false;
    DeleteAllowed = false;
    Caption = 'Renewable Contracts';



    layout
    {
        area(Content)
        {

            group(DateFilters)
            {
                ShowCaption = false;
                field(DateFilter; DateFilter)
                {
                    ApplicationArea = All;
                    Caption = 'Date Filter';
                    Editable = true;

                    trigger OnValidate()
                    var
                        myInt: Integer;
                    begin
                        TextManagement.MakeDateFilter(DateFilter);
                        SetFilter("Valid to Date", DateFilter);
                        DateFilter := GETFILTER("Valid to Date");
                        CurrPage.Update();
                    end;

                }

            }

            repeater(General)
            {
                Editable = false;

                field("Standard Sales Code"; "Standard Sales Code")
                {
                    Caption = 'Sales Code';
                    ApplicationArea = All;
                }
                field(CustomerNo; CustomerNo)
                {
                    Caption = 'Customer No.';
                }
                field(CustomerName; CustomerName)
                {
                    Caption = 'Name';
                }
                field("No."; "No.") { }
                field(Description; Description) { }
                field(Quantity; Quantity) { }
                field("Unit of Measure Code"; "Unit of Measure Code") { }
                field("Unit Price"; "Unit Price") { }
                field("Maintenance Percentage"; "Maintenance Percentage") { }
                field("Amount Excl. VAT"; "Amount Excl. VAT")
                {
                    Caption = 'Amount';
                }
                field("Line Discount %"; LineDiscounts)
                {
                    Caption = 'Line Discount %';
                }
                field("Net Amount"; "Net Amount")
                {
                    Caption = 'Net Amount';
                }
                field("Valid To date"; "Valid To date")
                {
                    Caption = 'Ending Date';
                }

            }

            group(Totals)
            {
                Editable = false;
                Caption = '  Totals';
                group(Control1)
                {
                    ShowCaption = false;
                    field(TotalAmount; TotalAmount)
                    {
                        Caption = 'Total Amount';
                        ShowCaption = true;
                    }
                }

                group(Control2)
                {
                    ShowCaption = false;
                    field(TotalAmountNet; TotalAmountNet)
                    {
                        Caption = 'Total Net Amount';
                        ShowCaption = true;

                    }
                }

            }

        }

    }

    actions
    {
        area(Processing)
        {
            action(OpenCard)
            {
                ApplicationArea = All;
                Caption = 'Open Card';
                ToolTip = 'View detailed contract information';
                Promoted = true;
                PromotedOnly = true;
                RunObject = page 50100;
                RunPageLink = Code = field("Standard Sales Code");
                Image = ViewDetails;

            }
            action(OpenCustomer)
            {
                ApplicationArea = All;
                Caption = 'Open Customer';
                ToolTip = 'View related customer card';
                Promoted = true;
                PromotedOnly = true;
                Image = Customer;
                trigger OnAction()
                var
                    ContractHeader: Record 172;
                    Customer: Record 18;
                begin
                    ContractHeader.SetRange(Code, rec."Standard Sales Code");
                    if ContractHeader.FindFirst then begin
                        if Customer.Get(ContractHeader."Customer No.") then
                            Page.Run(21, Customer)
                        else
                            Error('Customer was not found');
                    end;
                end;
            }

            action(Delete)
            {
                ApplicationArea = All;
                Image = Delete;
                Promoted = true;
                PromotedOnly = true;
                Caption = 'Delete';
                ToolTip = 'Delete the selected row';
                PromotedCategory = Process;

                trigger OnAction()
                var
                    CardPage: Page 50100;
                    Confirm: Dialog;
                    answer: Boolean;
                begin
                    answer := Dialog.Confirm('Delete %1?', true, Rec."Standard Sales Code");
                    if answer then begin
                        rec.Delete(true);
                    end;
                end;
            }
        }

    }



    trigger OnAfterGetRecord()
    var
        ContractHeader: Record 172;
        LineDiscount: Record 7004;
        Customer: Record 18;
        SalesHeader: Record 36 temporary;
        SalesLine: Record 37 temporary;
        TemplateHeader: Record 170;
    begin

        ContractHeader.SetRange(Code, rec."Standard Sales Code");
        if ContractHeader.FindFirst then
            CustomerNo := ContractHeader."Customer No.";
        if Customer.Get(ContractHeader."Customer No.") then
            CustomerName := Customer.Name;

        //Send temporary invoice
        SalesHeader."No." := '9999999999';
        SalesHeader."Document Type" := SalesHeader."Document Type"::Invoice;
        TemplateHeader.Get("Standard Sales Code");
        SalesHeader."Currency Code" := TemplateHeader."Currency Code";
        SalesHeader."Sell-to Customer No." := CustomerNo;
        SalesHeader."Bill-to Customer No." := CustomerNo;

        //SalesHeader.Insert(true);
        SalesLine."Document No." := '9999999999';
        SalesLine.Quantity := Rec.Quantity;
        SalesLine."No." := rec."No.";
        // SalesLine.Type := SalesLine.Type::Item;
        SalesLine.Type := Rec.Type;
        SalesLine."Document Type" := SalesLine."Document Type"::Invoice;
        SalesLine."Customer Disc. Group" := rec."Customer Discount Group";
        // if (rec.Type = rec.Type::Item) then
        //     SalesLine."Unit Price" := rec."Unit Price"
        // else begin
        // if Rec.Quantity <> 0 then
        //     SalesLine."Unit Price" := Rec."Amount Excl. VAT" / Quantity
        // else
        //     SalesLine."Unit Price" := Rec."Amount Excl. VAT";
        // end;
        SalesLine."Unit Price" := rec."Unit Price";

        PriceCalcMgt.FindSalesLineLineDisc(SalesHeader, SalesLine);


        LineDiscounts := SalesLine."Line Discount %";
        "Net Amount" := rec."Amount Excl. VAT" * (1 - SalesLine."Line Discount %" / 100);
        rec.Modify(true);

    end;

    trigger OnAfterGetCurrRecord()
    var
        TempRec: Record 171;
    begin

        TempRec := Rec;
        TempRec.SetFilter("Valid to Date", Rec.GetFilter("Valid to Date"));
        TempRec.SetFilter("Standard Sales Code", '*-*');
        TempRec.SetRange(Blocked, false);
        TempRec.CalcSums("Amount Excl. VAT", "Net Amount");
        TotalAmount := TempRec."Amount Excl. VAT";
        TotalAmountNet := TempRec."Net Amount";
    end;


    trigger OnOpenPage()
    var
        WDate: Date;
        Mdate: Date;
    begin
        FilterGroup(2);
        rec.SetFilter("Standard Sales Code", '*-*');
        FilterGroup(0);
        // DateFilter := Format(CalcDate('<CD>')) + '..' + Format(CalcDate('<CD+1M>'));//today
        WDate := WorkDate();
        Mdate := CalcDate('1M', WDate);
        DateFilter := Format(WDate) + '..' + Format(Mdate);//workday
        Rec.SetFilter("Valid to Date", DateFilter);
    end;


    var
        DateFilter: Text;
        TotalAmount: Decimal;
        AmountNet: Decimal;
        TotalAmountNet: Decimal;
        LineDiscounts: Decimal;
        PriceCalcMgt: Codeunit 7000;
        CustomerNo: Code[10];
        CustomerName: Text[100];
        TextManagement: Codeunit 41;
}