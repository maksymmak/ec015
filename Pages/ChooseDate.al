page 50101 "Choose New Date"
{
    PageType = StandardDialog;
    Caption = 'New Date';



    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field(Date; DateValue)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }

    }
    procedure GetDate(): Date
    var

    begin
        exit(DateValue);
    end;

    var
        DateValue: Date;
}