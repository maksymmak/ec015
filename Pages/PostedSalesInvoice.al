pageextension 50110 PostedSalesInvoice extends 132
{
    layout
    {
        addafter(Closed)
        {
            field("Recurring Sales Invoice"; "Recurring Sales Invoice")
            {
                Caption = 'Recurring Sales Invoice';
            }
            field("Standard Sales Code"; "Standard Sales Code")
            {
                Caption = 'Standard Sales Code';
            }
            field("Next recurring date"; "Next recurring date")
            {
                Caption = 'Next Recurring Date';
            }
        }

    }

    actions
    {
        // Add changes to page actions here
    }

    var
        myInt: Integer;
}