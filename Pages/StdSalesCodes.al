pageextension 50102 StandardSalesCodesExtension extends 172
{
    Editable = false;
    layout
    {
        addlast(Control1)
        {
            field("Maintenance Percentage"; "Maintenance Percentage")
            {
                Caption = 'Maintenance Percentage';
            }
            field(Template; Template)
            {
                Caption = 'Template';
                Visible = false;
            }
        }

    }
    actions
    {

        addfirst(Processing)
        {
            // action(ViewCard)
            // {
            //     ApplicationArea = All;
            //     Promoted = true;
            //     PromotedOnly = true;
            //     RunObject = page 50100;
            //     RunPageLink = Code = field(Code);
            //     Image = ViewDetails;
            //     Caption = 'View Card';

            //     trigger OnAction()
            //     begin

            //     end;
            // }
        }

    }

    trigger OnOpenPage()
    begin
        FilterGroup(2);
        Rec.SetRange(Template, false);
        FilterGroup(0);
    end;

}