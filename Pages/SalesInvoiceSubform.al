pageextension 50109 SalesInvoiceSubformExt extends 47
{
    layout
    {
        addlast(Control1)
        {
            field("Next Recurring Date"; "Next Recurring Date")
            {
                Caption = 'Next Recurring Date';
                Editable = isEditable;
            }
            field("Standard Sales Code"; "Standard Sales Code")
            {
                Caption = 'Standard Sales Code';
            }

        }
    }

    actions
    {

    }
    trigger OnAfterGetRecord()
    var

    begin
        if (rec."Standard Sales Code" <> '') then
            isEditable := true;
    end;

    var
        isEditable: Boolean;

}