pageextension 50103 StandardCustomerSalesCodesExt extends 173
{
    Editable = false;

    layout
    {
        addlast(Control1)
        {
            field("Recurring Frequency"; "Recurring Frequency")
            {
                ApplicationArea = All;
                Caption = 'Recurring Frequency';
            }
            field("Last Invoice Date"; "Last Invoice Date")
            {
                ApplicationArea = All;
                Caption = 'Last Invoice Date';

            }
            field("Next Invoice Date"; "Next Invoice Date")
            {
                ApplicationArea = All;
                Caption = 'Next Invoice Date';
            }
            field("Customer Discount Group"; "Customer Discount Group")
            {
                ApplicationArea = All;
                Caption = 'Customer Discount Group';
                Editable = false;
            }
            field("Maintenance Percentage"; "Maintenance Percentage")
            {
                ApplicationArea = All;
                Caption = 'Maintenance Percentage';
            }
            field("Deferral Code"; "Deferral Code")
            {
                ApplicationArea = All;
                Caption = 'Deferral Code';
            }

        }


    }

    actions
    {
        addlast(Processing)
        {

            action(ViewCard)
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                RunObject = page 50100;
                RunPageLink = Code = field(Code);
                Image = ViewDetails;
                Caption = 'View Card';
                ToolTip = 'View or change detailed information about the record';


                trigger OnAction()
                begin

                end;
            }
            action(NewContractFromTemplate)
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                Caption = 'Create New Contract';
                ToolTip = 'Create new contract from template';
                Image = NewRow;

                trigger OnAction()
                var
                    StandardSalesCodes: Page 50103;
                    StandardSalesCode: Record 170;
                    CustomerTransfer: Code[20];
                begin

                    CustomerTransfer := Rec."Customer No.";
                    if StrLen(CustomerTransfer) > 4 then
                        Error('Customer No. length should not exceed 4 digits');
                    StandardSalesCodes.SetTableView(StandardSalesCode);
                    StandardSalesCodes.LookupMode(true);
                    if StandardSalesCodes.RunModal = Action::LookupOK then begin
                        StandardSalesCodes.GetRecord(StandardSalesCode);
                        if StrLen(StandardSalesCode.Code) > 2 then
                            Error('Incorrect template');
                        CreateNewContractFromTemplate(StandardSalesCode.Code, CustomerTransfer);
                    end;
                end;
            }
            action(Delete)
            {
                ApplicationArea = All;
                Image = Delete;
                Promoted = true;
                PromotedOnly = true;
                Caption = 'Delete';
                ToolTip = 'Delete the selected row';
                PromotedCategory = Process;

                trigger OnAction()
                var
                    CardPage: Page 50100;
                    Confirm: Dialog;
                    answer: Boolean;
                begin
                    answer := Dialog.Confirm('Delete %1?', true, Rec.Code);
                    if answer then begin
                        // Page.RunModal(50100);
                        // CardPage.DeleteRecord(Code);
                        rec.Delete(true);
                        //Message('Contract %1 has been deleted', Rec.Code);
                    end;
                end;
            }
        }
        modify(Card)
        {
            Visible = false;
        }
    }
    procedure CreateNewContractFromTemplate(StandardSalesCode: Code[10]; CustomerNo: Code[20])
    var

    begin
        InsertContractFromTemplate(StandardSalesCode, InitContractCode(StandardSalesCode, CustomerNo), CustomerNo);
    end;

    local procedure InitContractCode(StandardSalesCode: Code[10]; CustomerNo: Code[20]): Code[30]
    var
        ContractCode: Code[30];
        NextCode: Code[10];
        TemplateHeader: Record 170;
        SearchCode: code[10];
    begin
        SearchCode := StandardSalesCode + '-' + CustomerNo + '-*';
        TemplateHeader.SetFilter(Code, SearchCode);
        if TemplateHeader.FindLast then begin
            ContractCode := IncStr(TemplateHeader.Code);
        end
        else
            ContractCode := StandardSalesCode + '-' + CustomerNo + '-01';
        exit(ContractCode);
    end;

    local procedure InsertContractFromTemplate(StandardSalesCode: Code[10]; NewContractCode: code[20]; CustomerNo: Code[20])
    var
        ContractHeader: Record 172;
        TemplateHeader: Record 170;
        TemplateHeaderNew: Record 170;
        ContractLine: Record 171;
        ContractLineNew: Record 171;
        Customer: Record 18;

    begin
        //Add new contract to the template table 
        if TemplateHeader.Get(StandardSalesCode) then begin
            //Create new record in the tamplate header 170
            TemplateHeaderNew := TemplateHeader;
            TemplateHeaderNew.Code := NewContractCode;
            TemplateHeaderNew.Template := false;

            //Get currency code from Customer card
            if Customer.Get("Customer No.") then
                TemplateHeaderNew."Currency Code" := Customer."Currency Code";
            TemplateHeaderNew.SetInsertAllowed(true);
            TemplateHeaderNew.Insert(true);
        end;
        //Add new contract to the Contract header 172
        if (TemplateHeader.Get(NewContractCode)) then begin
            ContractHeader.Init();
            ContractHeader.Code := NewContractCode;
            ContractHeader."Customer No." := CustomerNo;
            ContractHeader.Description := TemplateHeader.Description;
            ContractHeader."Maintenance Percentage" := TemplateHeader."Maintenance Percentage";
            if Customer.Get(CustomerNo) then
                ContractHeader."Customer Discount Group" := Customer."Customer Disc. Group";
            ContractHeader.Insert(true);
        end;

        //Copy Lines 

        //Add new record to the Line table

        ContractLine.SetRange("Standard Sales Code", StandardSalesCode);
        if ContractLine.FindSet then begin
            repeat
                with ContractLineNew do begin
                    ContractLineNew := ContractLine;
                    ContractLineNew."Standard Sales Code" := NewContractCode;
                    ContractLineNew."Customer Discount Group" := ContractHeader."Customer Discount Group";
                    ContractLineNew."Recurring Frequency" := ContractHeader."Recurring Frequency";
                    ContractLineNew."Header Frequency" := true;
                    Insert(true);
                end;
            until ContractLine.Next = 0;
        end;
        Commit();
        Page.RunModal(50100, ContractHeader);

    end;

    trigger OnOpenPage()
    var

    begin
        FilterGroup(2);
        rec.SetFilter(Code, '*-*');
    end;

}