page 50100 "Contract Card"
{
    PageType = Card;
    // ApplicationArea = All;
    //UsageCategory = Administration;
    SourceTable = 172;
    InsertAllowed = false;
    Caption = 'Contract Card';

    layout
    {
        area(Content)
        {
            group(General)
            {
                field(Code; Code)
                {
                    ApplicationArea = All;
                    Editable = false;
                    Lookup = false;

                }
                field(Description; Description)
                {
                    ApplicationArea = All;

                }
                field("Maintenance Percentage"; "Maintenance Percentage")
                {
                    ApplicationArea = All;
                    Editable = false;

                }
                field(CurrencyCode; CurrencyCode)
                {
                    Caption = 'Currency Code';
                    ApplicationArea = All;
                    Editable = false;

                }
                field("Valid From Date"; "Valid From Date")
                {
                    ApplicationArea = All;

                }
                field("Valid To Date"; "Valid To date")
                {
                    Caption = 'Valid To Date';
                    ApplicationArea = All;

                }
                field("Recurring Frequency"; "Recurring Frequency")
                {
                    ApplicationArea = All;
                    ShowMandatory = true;

                }
                field("Last Invoice Date"; "Last Invoice Date")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("Next Invoice Date"; "Next Invoice Date")
                {
                    ApplicationArea = All;

                }
                field("Customer Discount Group"; "Customer Discount Group")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("Deferral Code"; "Deferral Code")
                {
                    ApplicationArea = All;

                }
                field(Blocked; Blocked)
                {
                    ApplicationArea = all;
                }

            }
            part("Standard Sales Code Subform"; "Standard Sales Code Subform")
            {
                SubPageLink = "Standard Sales Code" = field(Code);
            }
        }
        area(FactBoxes)
        {

        }

    }

    actions
    {

    }
    trigger OnAfterGetCurrRecord()
    var
        StdSalesCode: Record 170;
    begin
        StdSalesCode.Get(Rec.Code);
        CurrencyCode := StdSalesCode."Currency Code";
    end;

    procedure DeleteRecord(StdCode: code[10])
    var

    begin
        Rec.SetRange(Code, StdCode);
        if rec.FindFirst then
            Rec.Delete(true);
    end;

    var
        CurrencyCode: Code[10];


}