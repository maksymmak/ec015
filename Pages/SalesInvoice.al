pageextension 50108 SalesInvoiceExt extends 43
{
    layout
    {
        addafter(Status)
        {
            field("Recurring Sales Invoice"; "Recurring Sales Invoice")
            {
                Caption = 'Recurring Sales Invoice';
                // Visible = isVisible;
            }
            field("Standard Sales Code"; "Standard Sales Code")
            {
                Caption = 'Standard Sales Code';
                //Visible = isVisible;
            }
            field("Next recurring date"; "Next recurring date")
            {
                Caption = 'Next Recurring Date';
                // Visible = isVisible;
                Editable = isEditable;
            }
        }

    }

    actions
    {
        modify(GetRecurringSalesLines)
        {
            Visible = false;
        }
    }
    trigger OnAfterGetCurrRecord()
    var

    begin
        if "Recurring Sales Invoice" then
            isEditable := true;
    end;


    var
        isEditable: Boolean;
    //isVisible: Boolean;

}