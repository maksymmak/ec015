pageextension 50106 CustomerListExt extends 22
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addbefore("Recurring Sales Lines")
        {
            action(ViewTemplates)
            {
                ApplicationArea = All;
                Caption = 'Templates';
                ToolTip = 'Manage templates';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Category8;
                RunObject = page 50103;
                Image = Documents;

                trigger OnAction()
                begin

                end;
            }
            action(RenuwableContracts)
            {
                ApplicationArea = All;
                Caption = 'Renewable Contracts';
                ToolTip = 'View the list of all contracts';
                Promoted = true;
                PromotedCategory = Category8;
                RunObject = page 50102;
                Image = AmountByPeriod;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}