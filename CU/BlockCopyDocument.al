codeunit 50104 BlockCopyDocument
{
    [EventSubscriber(ObjectType::CodeUnit, 6620, 'OnBeforeUpdateSalesLine', '', true, true)]
    local procedure BlockCopyDocument(var FromSalesHeader: Record "Sales Header")
    begin
        if FromSalesHeader."Recurring Sales Invoice" then
            Error('Operation is not available for Recurring Sales Invoice.');
    end;

    [EventSubscriber(ObjectType::CodeUnit, 6620, 'OnBeforeCopySalesLinesToDoc', '', true, true)]
    local procedure BlockCreditMemo(var FromSalesInvoiceLine: Record "Sales Invoice Line")
    begin
        if (FromSalesInvoiceLine."Standard Sales Code" <> '') then
            Error('Operation is not available for Recurring Sales Invoice.');
    end;

}