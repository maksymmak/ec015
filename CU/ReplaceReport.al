codeunit 50102 "ReplaceCreateRecurringSalesInv"
{
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure OnAfterSubstituteReport(ReportId: Integer; var NewReportId: Integer)
    begin
        if ReportId = Report::"Create Recurring Sales Inv." then
            NewReportId := Report::"Create Recurring Sales Inv.NEW";
    end;
}