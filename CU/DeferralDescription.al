codeunit 50103 DeferralDescription
{
    EventSubscriberInstance = StaticAutomatic;

    [EventSubscriber(ObjectType::Codeunit, 1720, 'OnAfterCreateDeferralSchedule', '', true, true)]
    local procedure MyProcedure(var DeferralLine: Record "Deferral Line"; DeferralHeader: Record "Deferral Header")
    var
        SalesLine: Record 37;
        CDescription: Text[250];
        ContractHeader: Record 172;
        DeferralLines: Record 1702;

    begin
        SalesLine.SetRange("Document No.", DeferralHeader."Document No.");
        SalesLine.SetRange("Line No.", DeferralHeader."Line No.");
        if SalesLine.FindFirst() and (SalesLine."Standard Sales Code" <> '') then begin

            ContractHeader.SetRange(Code, SalesLine."Standard Sales Code");
            if ContractHeader.FindFirst() then
                CDescription := ContractHeader.Description;
            DeferralLines.SetRange("Document No.", SalesLine."Document No.");
            DeferralLines.SetRange("Document Type", SalesLine."Document Type");
            DeferralLines.SetRange("Line No.", SalesLine."Line No.");
            if DeferralLines.FindSet then
                DeferralLines.ModifyAll(Description, CDescription);
        end;

    end;




}