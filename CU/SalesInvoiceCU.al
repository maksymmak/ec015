codeunit 50100 EventSubForCreateInvoice
{
    EventSubscriberInstance = StaticAutomatic;
    //Change header
    [EventSubscriber(ObjectType::Table, 172, 'OnAfterCreateSalesInvoice', '', true, true)]
    local procedure ModifySalesInvoice(var SalesHeader: Record "Sales Header"; StandardCustomerSalesCode: Record "Standard Customer Sales Code")
    var
    begin
        SalesHeader."Standard Sales Code" := StandardCustomerSalesCode.Code;
        SalesHeader."Recurring Sales Invoice" := true;
        SalesHeader."Posting Description" := StandardCustomerSalesCode.Description;
        SalesHeader."Next recurring date" := CalcDate(StandardCustomerSalesCode."Recurring Frequency", SalesHeader."Posting Date");
        SalesHeader.Modify(true);
    end;

    //Insert first two lines
    [EventSubscriber(ObjectType::Table, 172, 'OnBeforeApplyStdCodesToSalesLinesLoop', '', true, true)]
    local procedure InsertFirstTwoLines(var SalesLine: Record "Sales Line"; SalesHeader: Record "Sales Header"; StdSalesCode: Record "Standard Sales Code"; var StdSalesLine: Record "Standard Sales Line")
    var
        d: Date;
        NewDate: Record Date;
        CustomerHeadr: Record 172;
    //test posible bugfix
    //SalesHeaderSub: Record 36;

    begin
        //test posible bugfix
        // if SalesHeaderSub.Get(SalesHeader."Document Type", SalesHeader."No.") then begin
        //     CustomerHeadr.SetRange(Code, StdSalesCode.Code);
        //     if CustomerHeadr.FindFirst then begin
        //         SalesHeaderSub."Posting Description" := CustomerHeadr.Description;
        //         SalesHeaderSub."Standard Sales Code" := CustomerHeadr.Code;
        //         SalesHeaderSub."Next recurring date" := CalcDate(CustomerHeadr."Recurring Frequency", SalesHeader."Posting Date");
        //         SalesHeaderSub."Recurring Sales Invoice" := true;
        //         SalesHeaderSub.Modify();
        //     end;
        // end;
        //END test posible bugfix

        CustomerHeadr.SetRange(Code, StdSalesCode.Code);
        if not CustomerHeadr.FindFirst then
            Error('Standard customer sales code was not found');
        //Filter based on valid date
        StdSalesLine.SetFilter("Valid From Date", '%1|<=%2', 0D, SalesHeader."Order Date");
        StdSalesLine.SetFilter("Valid To date", '%1|>=%2', 0D, SalesHeader."Order Date");
        //End filter on valid date
        StdSalesLine.SetFilter("Next Invoice Date", CustomerHeadr.DateFilter);
        StdSalesLine.SetRange(Blocked, false);
        StdSalesLine.SetRange("Header Frequency", true);
        if StdSalesLine.FindSet() then
            repeat
                StdSalesLine.Mark(true);
            until StdSalesLine.Next() = 0;

        StdSalesLine.SetRange("Header Frequency", false);
        if StdSalesLine.FindSet() then
            repeat
                d := CalcDate(StdSalesLine."Recurring Frequency", SalesHeader."Posting Date");
                NewDate.SetFilter("Period Start", CustomerHeadr.DateFilter);
                NewDate.FilterGroup(2);
                NewDate.SetRange("Period Start", d);
                if not NewDate.IsEmpty then
                    StdSalesLine.Mark(true);

            until StdSalesLine.Next() = 0;
        StdSalesLine.SetRange("Header Frequency");
        StdSalesLine.MarkedOnly;

        if StdSalesLine.FindFirst then begin
            //Add description
            SalesLine."Document Type" := SalesLine."Document Type"::Invoice;
            SalesLine."Document No." := SalesHeader."No.";
            SalesLine."Line No." := GetNextLineNo(SalesLine);
            SalesLine.Description := CustomerHeadr.Description;
            SalesLine.Insert(true);
            //Period to which the invoice applies
            SalesLine."Document Type" := SalesLine."Document Type"::Invoice;
            SalesLine."Document No." := SalesHeader."No.";
            SalesLine."Line No." := GetNextLineNo(SalesLine);
            //Period to wich invoice applies
            SalesLine.Description := Format(SalesHeader."Posting Date") + '..' + Format(CalcDate(StdSalesLine."Recurring Frequency", SalesHeader."Posting Date"));
            SalesLine.Insert(true);
        end;



    end;

    [EventSubscriber(ObjectType::Table, 172, 'OnBeforeApplyStdCodesToSalesLines', '', true, true)]
    procedure MyProcedure(var SalesLine: Record "Sales Line"; StdSalesLine: Record "Standard Sales Line")
    var
        SalesLine2: Record 37;
        TimeFrame: Date;
        ContractHeader: Record 172;
        SalesHeader: Record 36;

    begin

        //Description for header frequency
        // if not StdSalesLine."Header Frequency" then begin
        //     //New next invoice date on line
        //     //frequency from line
        //     ContractHeader.SetRange(Code, StdSalesLine."Standard Sales Code");
        //     if not ContractHeader.FindFirst then
        //         Error('Errror');

        //     SalesHeader.Get(SalesHeader."Document Type"::Invoice, SalesLine."Document No.");

        //     SalesLine2."Document Type" := SalesLine."Document Type"::Invoice;
        //     SalesLine2."Document No." := SalesLine."Document No.";
        //     SalesLine2.Description := Format(SalesHeader."Posting Date") + '..' + Format(CalcDate(StdSalesLine."Recurring Frequency", SalesHeader."Posting Date"));
        //     SalesLine2."Line No." := GetNextLineNo(SalesLine2);
        //     SalesLine2.Insert(true);
        // end;
        //Setrange

        //Get posting date
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Invoice);
        SalesHeader.SetRange("No.", SalesLine."Document No.");
        if SalesHeader.FindFirst() then
            SalesLine."Next Recurring Date" := CalcDate(Format(StdSalesLine."Recurring Frequency"), SalesHeader."Posting Date");


        SalesLine."Standard Sales Code" := StdSalesLine."Standard Sales Code";
        SalesLine."Standard Sales Line No." := StdSalesLine."Line No.";


        ContractHeader.SetRange(Code, StdSalesLine."Standard Sales Code");
        if ContractHeader.FindFirst then
            SalesLine.Validate("Deferral Code", ContractHeader."Deferral Code");
        //Copied from letter
        SalesLine.Validate("Customer Disc. Group", StdSalesLine."Customer Discount Group");
        if StdSalesLine.Quantity <> 0 then
            SalesLine.Validate("Unit Price", (StdSalesLine."Amount Excl. VAT" / StdSalesLine.Quantity))
        else
            SalesLine.Validate("Unit Price", (StdSalesLine."Amount Excl. VAT"));

    end;

    local procedure GetNextLineNo(SalesLine: Record "Sales Line"): Integer
    var

    begin
        SalesLine.SetRange("Document Type", SalesLine."Document Type");
        SalesLine.SetRange("Document No.", SalesLine."Document No.");
        if SalesLine.FindLast then
            exit(SalesLine."Line No." + 10000);

        exit(10000);

    end;


    var
        ContractHeader: Record 172;
        DeferralCode: Code[20];
}