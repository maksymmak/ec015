tableextension 50102 StandardCustomerSalesCodeExt extends 172
{

    fields
    {

        field(50000; "Recurring Frequency"; DateFormula)
        {
            Caption = 'Recurring Frequency';
            //NotBlank = true;
            trigger OnValidate()
            var
            begin
                ContractLine.SetRange("Standard Sales Code", Code);
                ContractLine.SetRange("Header Frequency", true);
                if ContractLine.FindSet then begin
                    repeat
                        ContractLine."Recurring Frequency" := "Recurring Frequency";
                        ContractLine.Modify();
                    until ContractLine.Next = 0
                end;
            end;

        }
        field(50001; "Last Invoice Date"; Date)
        {
            Caption = 'Last Invoice Date';
            FieldClass = FlowField;
            CalcFormula = max ("Sales Invoice Header"."Posting Date" where("Standard Sales Code" = field("Code")));

        }
        field(50002; "Next Invoice Date"; Date)
        {
            Caption = 'Next Invoice Date';
            FieldClass = FlowField;
            CalcFormula = max ("Sales Invoice Header"."Next recurring date" where("Standard Sales Code" = field("Code")));

        }
        field(50003; "Customer Discount Group"; Code[10])
        {
            Caption = 'Customer Discount Group';
            TableRelation = "Customer Discount Group".Code;
            Editable = false;
            //posible new solution
            // FieldClass = FlowField;
            // CalcFormula = lookup (Customer."Customer Disc. Group" where("No." = field("Customer No.")));
            //end
        }
        field(50004; "Maintenance Percentage"; Decimal)
        {
            Caption = 'Maintenance Percentage';
            //Editable = false;

        }
        field(50005; "Deferral Code"; Code[10])
        {
            Caption = 'Deferral Code';
            TableRelation = "Deferral Template"."Deferral Code";
        }
        field(50006; "DateFilter"; Text[100])
        {
            Caption = 'DateFilter';
        }

        modify("Valid From Date")
        {
            trigger OnAfterValidate()
            var
            begin
                ContractLine.SetRange("Standard Sales Code", Code);
                if ContractLine.FindSet then begin
                    repeat
                        ContractLine."Valid From Date" := "Valid From Date";
                        ContractLine.Modify();
                    until ContractLine.Next = 0
                end;
            end;
        }
        modify("Valid To date")
        {
            trigger OnAfterValidate()
            var
            begin
                ContractLine.SetRange("Standard Sales Code", Code);
                if ContractLine.FindSet then begin
                    repeat
                        ContractLine."Valid to Date" := "Valid To date";
                        ContractLine.Modify();
                    until ContractLine.Next = 0
                end;
            end;
        }
        modify(Blocked)
        {
            trigger OnAfterValidate()
            var
            begin
                //if Blocked = true then begin
                ContractLine.SetRange("Standard Sales Code", Code);
                if ContractLine.FindSet then begin
                    repeat
                        ContractLine.Blocked := Blocked;
                        ContractLine.Modify();
                    until ContractLine.Next = 0
                end;
            end;
            //end;
        }


    }
    trigger OnInsert()
    var
        Customer: Record 18;
    begin
        if Customer.Get("Customer No.") then begin
            "Customer Discount Group" := Customer."Customer Disc. Group";
        end;

    end;


    trigger OnDelete()
    var
        TemplateHeader: Record 170;
        TemplateLines: Record 171;
        SalesHeader: Record 36;
        SalesInvoiceHeader: Record 112;
    begin
        //Check invoice
        SalesHeader.SetRange("Standard Sales Code", Rec.Code);
        if SalesHeader.FindFirst then
            Error('Invoice was created for that contract');
        SalesInvoiceHeader.SetRange("Standard Sales Code", rec.Code);
        if SalesInvoiceHeader.FindFirst then
            Error('Invoice was created for that contract');

        //Delete from template
        if TemplateHeader.Get(rec.Code) then
            TemplateHeader.Delete(true);
    end;

    var
        ContractLine: Record 171;


}