tableextension 50107 StandardSalesLineExtension extends 171
{
    fields
    {
        field(50000; "Unit Price"; Decimal)
        {
            Caption = 'Unit Price';
        }
        field(50001; "Maintenance Percentage"; Decimal)
        {
            Caption = 'Maintenance Percentage';

        }
        field(50002; "Customer Discount Group"; Code[10])
        {
            Caption = 'Customer Discount Group';

        }
        field(50003; "Valid From Date"; Date)
        {
            Caption = 'Valid From Date';

        }
        field(50004; "Valid to Date"; Date)
        {
            Caption = 'Valid to Date';
        }
        field(50005; "Recurring Frequency"; DateFormula)
        {
            Caption = 'Recurring Frequency';
            NotBlank = true;
        }
        field(50006; "Last Invoice Date"; Date)
        {
            Caption = 'Last Invoice Date';
            FieldClass = FlowField;
            CalcFormula = max ("Sales Invoice Line"."Posting Date" where
            ("Standard Sales Code" = field("Standard Sales Code"), "Standard Sales Line No." = field("Line No.")));

        }
        field(50007; "Next Invoice Date"; Date)
        {
            Caption = 'Next Invoice Date';
            FieldClass = FlowField;
            CalcFormula = max ("Sales Invoice Line"."Next recurring date" where
            ("Standard Sales Code" = field("Standard Sales Code"), "Standard Sales Line No." = field("Line No.")));
        }
        field(50008; "Header Frequency"; Boolean)
        {
            Caption = 'Header Frequency';
        }
        field(50009; Blocked; Boolean)
        {
            Caption = 'Blocked';

        }
        field(50010; "Net Amount"; Decimal)
        {
            Caption = 'Net Amount';
        }
        modify(Quantity)
        {
            trigger OnAfterValidate()
            var
                Item: Record 27;
                StandardSalesCode: Record 170;
                ContractHeader: Record 172;
            begin
                if "Maintenance Percentage" <> 0 then
                    "Amount Excl. VAT" := ("Unit Price" * ("Maintenance Percentage" / 100)) * Quantity
                else
                    "Amount Excl. VAT" := "Unit Price" * Quantity;
            end;
        }
        modify(Type)
        {
            trigger OnBeforeValidate()
            var
                PostedSalesLines: Record 113;
                SalesLines: Record 37;
            begin
                PostedSalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
                PostedSalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
                if PostedSalesLines.FindFirst then
                    Error('This line was posted and can`t be modified');
                SalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
                SalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
                if SalesLines.FindFirst then
                    Error('This line has sales invoice and can`t be modified');
            end;
        }
        modify("No.")
        {
            trigger OnBeforeValidate()
            var
                PostedSalesLines: Record 113;
                SalesLines: Record 37;
            begin
                PostedSalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
                PostedSalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
                if PostedSalesLines.FindFirst then
                    Error('This line was posted and can`t be modified');
                SalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
                SalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
                if SalesLines.FindFirst then
                    Error('This line has sales invoice and can`t be modified');
            end;
        }

    }

    trigger OnBeforeDelete()
    var
        PostedSalesLines: Record 113;
        SalesLines: Record 37;
    begin
        PostedSalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
        PostedSalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
        if PostedSalesLines.FindFirst then
            Error('This line was posted and can`t be deleted');
        SalesLines.SetRange("Standard Sales Code", rec."Standard Sales Code");
        SalesLines.SetRange("Standard Sales Line No.", rec."Line No.");
        if SalesLines.FindFirst then
            Error('This line has sales invoice and can`t be deleted');

    end;
}