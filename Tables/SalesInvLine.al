tableextension 50104 SalesInvoiceLineExtension extends 113
{
    fields
    {
        field(50001; "Next recurring date"; Date)
        {

        }
        field(50002; "Standard Sales Code"; code[20])
        {
            Caption = 'Standard Sales Code';
            TableRelation = "Standard Sales Code".Code;
        }
        field(50003; "Standard Sales Line No."; Integer)
        {
            Caption = 'Standard Sales Line No';
            Editable = false;
        }
    }

}