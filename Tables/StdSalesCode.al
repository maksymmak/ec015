tableextension 50101 StandardSalesCodeExtension extends 170
{
    fields
    {
        field(50000; "Maintenance Percentage"; Decimal)
        {
            Caption = 'Maintenance Percentage';
            MinValue = 0;
            MaxValue = 100;
            trigger OnValidate()
            var

            begin
                if not rec.Template then
                    Error('Maintenance Percentage can`t be changed');
            end;

        }
        field(50001; Template; Boolean)
        {
            Caption = 'Template';

        }


    }
    trigger OnRename()
    var
        myInt: Integer;
    begin
        if not rec.Template then
            Error('Code can`t be changed');
    end;
    //NEW CODE
    trigger OnBeforeInsert()
    begin
        if not InsertAllowedG then
            if not Template then
                Error('Templates can only be created from coresponded page');

    end;

    procedure SetInsertAllowed(InsertAllowed: Boolean)
    begin
        InsertAllowedG := InsertAllowed;
    end;

    var
        InsertAllowedG: Boolean;
    //END NEW CODE
}