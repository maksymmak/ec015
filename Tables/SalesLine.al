tableextension 50106 SalesLineExtension extends 37
{
    fields
    {
        field(50001; "Next Recurring Date"; Date)
        {
            Caption = 'Next Recurring Date';
            //Editable = false;
        }
        field(50002; "Standard Sales Code"; Code[20])
        {
            Caption = 'Standard Sales Code';
            Editable = false;

        }
        field(50003; "Standard Sales Line No."; Integer)
        {
            Caption = 'Standard Sales Line No';
            Editable = false;
        }
        modify("Deferral Code")
        {
            trigger OnAfterValidate()
            var
                DeferralLines: Record 1702;
                ContractHeader: Record 172;
                CDescription: Text[250];
            begin
                // //Message(rec."Document No.");
                // DeferralLines.SetRange("Document No.", rec."Document No.");
                // //DeferralLines.SetRange("Document Type", Rec."Document Type");
                // DeferralLines.SetRange("Line No.", Rec."Line No.");
                // if DeferralLines.FindSet then
                //     DeferralLines.ModifyAll(Description, Rec."Standard Sales Code");

                ContractHeader.SetRange(Code, Rec."Standard Sales Code");
                if ContractHeader.FindFirst() then
                    CDescription := ContractHeader.Description;

                DeferralLines.SetRange("Document No.", rec."Document No.");
                DeferralLines.SetRange("Document Type", Rec."Document Type");
                DeferralLines.SetRange("Line No.", Rec."Line No.");
                if DeferralLines.FindSet then
                    DeferralLines.ModifyAll(Description, CDescription);
            end;
        }

    }
    trigger OnAfterInsert()
    var
        DeferralLines: Record 1702;
        ContractHeader: Record 172;
        CDescription: Text[250];
    begin
        ContractHeader.SetRange(Code, Rec."Standard Sales Code");
        if ContractHeader.FindFirst() then
            CDescription := ContractHeader.Description;

        DeferralLines.SetRange("Document No.", rec."Document No.");
        DeferralLines.SetRange("Document Type", Rec."Document Type");
        DeferralLines.SetRange("Line No.", Rec."Line No.");
        if DeferralLines.FindSet then
            DeferralLines.ModifyAll(Description, CDescription);
    end;

    trigger OnBeforeModify()
    var
        myInt: Integer;
    begin

    end;



}