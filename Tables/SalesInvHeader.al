tableextension 50103 SalesInvoiceHeaderExtension extends 112
{
    fields
    {
        field(50000; "Recurring Sales Invoice"; Boolean)
        {
            Editable = false;
        }
        field(50002; "Next recurring date"; Date)
        {
            Editable = false;
        }
        field(50001; "Standard Sales Code"; code[20])
        {
            Caption = 'Standard Sales Code';
            Editable = false;
            TableRelation = "Standard Sales Code";
            trigger OnLookup()
            var
                ContractHeader: Record 172;
            begin
                ContractHeader.SetRange(Code, Rec."Standard Sales Code");
                page.Run(50100, ContractHeader);
            end;
        }


    }

}