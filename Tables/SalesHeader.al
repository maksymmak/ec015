tableextension 50105 SalesHeaderExtension extends 36
{
    fields
    {
        field(50000; "Recurring Sales Invoice"; Boolean)
        {
            Caption = 'Recurring Sales Invoice';
            Editable = false;
        }
        field(50001; "Standard Sales Code"; Code[20])
        {
            Caption = 'Standard Sales Code';
            Editable = false;
            TableRelation = "Standard Sales Code";

            trigger OnLookup()
            var
                ContractHeader: Record 172;
            begin
                ContractHeader.SetRange(Code, Rec."Standard Sales Code");
                page.Run(50100, ContractHeader);
            end;
        }
        field(50002; "Next recurring date"; Date)
        {
            trigger OnValidate()
            var
                SalesLines: Record 37;
                ContractLines: Record 171;
            begin
                //Change date for all lines
                // SalesLines.SetRange("Document No.", "No.");
                // SalesLines.SetRange("Standard Sales Code", Rec."Standard Sales Code");
                // SalesLines.ModifyAll("Next Recurring Date", rec."Next recurring date");
                //End change date for all lines

                //Change for lines with header frequency
                SalesLines.SetRange("Document No.", "No.");
                SalesLines.SetRange("Standard Sales Code", Rec."Standard Sales Code");
                if SalesLines.FindSet then begin
                    with SalesLines do begin
                        if ContractLines.Get(SalesLines."Standard Sales Code", SalesLines."Standard Sales Line No.")
                            and ContractLines."Header Frequency" then
                            SalesLines."Next Recurring Date" := rec."Next recurring date";
                        SalesLines.Modify();
                    end;
                end;
                //End with header fr
            end;

        }
        modify("Posting Date")
        {
            trigger OnAfterValidate()
            var
                ContractHeader: Record 172;
                SalesLines: Record 37;
                StdSalesLine: Record 171;
            begin
                ContractHeader.SetRange(Code, Rec."Standard Sales Code");
                if ContractHeader.FindFirst() then begin
                    "Next recurring date" := CalcDate(ContractHeader."Recurring Frequency", "Posting Date");
                    Modify(true);
                    Commit();
                    SalesLines.SetRange("Document No.", Rec."No.");
                    SalesLines.SetRange("Standard Sales Code", Rec."Standard Sales Code");
                    if SalesLines.FindFirst() then
                        repeat
                            StdSalesLine.Get(rec."Standard Sales Code", SalesLines."Standard Sales Line No.");
                            SalesLines."Next Recurring Date" := CalcDate(Format(StdSalesLine."Recurring Frequency"), Rec."Posting Date");
                            SalesLines.Modify();
                        until SalesLines.Next() = 0;
                end;
            end;
        }
    }
    var
        SalesInvoiceCu: Codeunit 50100;
}