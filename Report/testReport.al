Report 50100 "Create Recurring Sales Inv.NEW"
{
    //AdditionalSearchTerms = 'repeat sales';
    // ApplicationArea = Basic, Suite;
    // Caption = 'Create Recurring Sales Invoices DEV';
    ProcessingOnly = true;
    // UsageCategory = Tasks;

    dataset
    {
        dataitem("Standard Customer Sales Code"; "Standard Customer Sales Code")
        {
            RequestFilterFields = "Customer No.", "Code";
            column(ReportForNavId_1; 1)
            {
            }

            trigger OnAfterGetRecord()
            var
                SalesInvoice: Record 36;
            begin
                SalesInvoice.SetRange("Standard Sales Code", Code);
                if SalesInvoice.FindFirst then
                    Error('There are unposted recurring sales invoices that should be deleted before proceeding');
                Counter += 1;
                Window.Update(1, 10000 * Counter DIV TotalCount);

                //Check if there is any lines
                ContractLines.SetRange("Standard Sales Code", Code);

                //Check Valid to Date
                ContractLines.SetFilter("Valid From Date", '%1|<=%2', 0D, OrderDate);
                ContractLines.SetFilter("Valid To date", '%1|>=%2', 0D, OrderDate);
                //End check

                ContractLines.SetFilter("Next Invoice Date", '%1..%2|%3', FromDate, ToDate, 0D);
                DateFilter := ContractLines.GetFilter("Next Invoice Date");
                ContractLines.SetFilter("Next Invoice Date", DateFilter);
                ContractLines.SetRange(Blocked, false);
                Modify(true);

                if ContractLines.FindFirst then begin
                    CreateSalesInvoice(OrderDate, PostingDate);
                    NumberOfInvoices += 1;
                end;

            end;

            trigger OnPreDataItem()
            var

            begin
                // SetFilter("Valid From Date", '%1|<=%2', 0D, OrderDate);
                // SetFilter("Valid To date", '%1|>=%2', 0D, OrderDate);
                SetRange(Blocked, false);
                TotalCount := Count;
                Window.Open(ProgressMsg);
            end;

        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(OrderDate; OrderDate)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Order Date';
                    ToolTip = 'Specifies the date that will be entered in the Document Date field on the sales invoices that are created by using the batch job.';
                }
                field(PostingDate; PostingDate)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Posting Date';
                    ToolTip = 'Specifies the date that will be entered in the Posting Date field on the sales invoices that are created by using the batch job.';
                }
                field(FromDate; FromDate)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Start Date';
                    ToolTip = 'Specifies the start date of invoicing';
                }
                field(ToDate; ToDate)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'End Date';
                    ToolTip = 'Specifies the end date of invoicing';
                }


            }

        }

        actions
        {
        }
        trigger OnOpenPage()
        var
            WDate: Date;
            Mdate: Date;

        begin
            WDate := WorkDate();
            FromDate := CalcDate('-CM', WDate);
            ToDate := CalcDate('CM', WDate);

            // FromDate := CalcDate('<-CM>');
            // PostingDate := CalcDate('<-CM>');
            // OrderDate := CalcDate('<-CM>');
            // ToDate := CalcDate('<CM>');
        end;
    }

    labels
    {
    }


    trigger OnPostReport()
    begin
        Window.Close;
        Message(NoOfInvoicesMsg, NumberOfInvoices);
    end;

    trigger OnPreReport()
    begin
        if (OrderDate = 0D) or (PostingDate = 0D) or (ToDate = 0D) then
            Error(MissingDatesErr);
    end;

    var
        Window: Dialog;
        ContractLines: Record 171;
        PostingDate: Date;
        OrderDate: Date;
        FromDate: Date;
        ToDate: Date;
        MissingDatesErr: label 'You must enter a posting date, an order date, and an end date';
        TotalCount: Integer;
        Counter: Integer;
        ProgressMsg: label 'Creating Invoices #1##################';
        NoOfInvoicesMsg: label '%1 invoices were created.';
        NumberOfInvoices: Integer;
}

